data_input = str(input())

arr_values = []
prev = ""
dict_values = {}
pos = 0
for x in data_input:
    pos += 1
    if prev == "":
        dict_values[x] = 1
    else:
        if prev == str(x):
            dict_values[x] += 1
        else:
            arr_values.append(dict_values)
            dict_values = {x: 1}
    if pos == len(data_input):
        arr_values.append(dict_values)
    prev = x

str_out = ""
for value in arr_values:
    str_out += "(" + str(list(value.values())[0]) + ", " + str(list(value.keys())[0]) + ") "

print(str_out)

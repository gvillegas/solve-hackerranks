numeros = [2, 1, 1, 1, 3, 3, 2, 2, 2, 1, 1, 1, 1, 1, 3, 3]
#numeros = [1, 2, 2, 3, 3, 3, 1, 1, 1]
numeros = [1, 1, 2, 3, 3, 3, 10, 10]
numeros = [1, 1, 2, 2, 2, 1, 1, 1]
print('IN:',numeros)
def nrosConsecutivos(arr):
    consecutivos = []
    count = 0
    pos = 0
    last = 0
    for x in arr:
        current = x
        if pos != 0:
            if current != last:
                consecutivos.append(count)
                consecutivos.append(last)
                count = 0

            if arr.__len__() == pos + 1:
                consecutivos.append(count + 1)

                if current != last:
                    consecutivos.append(current)
                else:
                    consecutivos.append(last)

        count += 1
        last = current
        pos += 1
    return consecutivos

print('OUT:', nrosConsecutivos(numeros))
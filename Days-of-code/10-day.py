#!/bin/python3

import sys


n = int(input().strip())

def consecutivos(arr):
    pos = 0
    count = 0
    maxcount= 0
    for x in arr:
        if pos > 0:
            if arr[pos-1] == x:
                count += 1

            if arr[pos-1] != x:
                if maxcount < count:
                    maxcount = count
                count = 1

        else:
            count += 1
        if arr.__len__()-1 == pos:
            if maxcount < count:
                maxcount = count
        pos += 1

    return maxcount

print(consecutivos(bin(n)[2:]))
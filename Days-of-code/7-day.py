#!/bin/python3

import sys


n = int(input().strip())
arr = [int(arr_temp) for arr_temp in input().strip().split(' ')]
arr_temp = []

while arr.__len__() > 0:
    arr_temp.append(arr.pop())

print(" ".join(str(x) for x in arr_temp))
#!/bin/python

import sys


N = int(raw_input().strip())

if (N%2)>0:
    print('Weird')
    exit(0)
    
if (N%2)==0:
    if 6 <= N <= 20:
        print('Weird')
    
    if N > 20 or 2 <= N <= 5:
        print('Not Weird')
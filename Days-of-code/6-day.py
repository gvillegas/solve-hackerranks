def even(string):  # pares
    str_temp = ""
    for x in range(0, string.__len__()):
        if x == 0:
            str_temp += string[x]
        elif x % 2 == 0:
            str_temp += string[x]
    return str_temp


def odd(string):  # impares
    str_temp = ""
    for y in range(0, string.__len__()):
        if y > 0 and y % 2 > 0:
            str_temp += string[y]

    return str_temp


cant = int(input())  # Cantidad de strings
strings = []

for i in range(0, cant):
    strings.append(str(input()))

for a in strings:
    print(even(a), odd(a))
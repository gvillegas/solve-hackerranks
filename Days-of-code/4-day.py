class Person:
    def __init__(self, initialAge):
        if initialAge < 0:
            print('Age is not valid, setting age to 0.')
            self._age = 0
        else:
            self._age = initialAge
            # Add some more code to run some checks on initialAge
    
    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, age):
        self._age = age
    
    def amIOld(self):
        # Do some computations in here and print out the correct statement to the console
        if self.age < 13:
            print('You are young.')
        
        if self.age >= 13 and self.age < 18:
            print('You are a teenager.')
        
        if self.age >= 18:
            print('You are old.')
        
    def yearPasses(self):
        # Increment the age of the person in here
        self._age += 1
        
t = int(raw_input())
for i in range(0, t):
    age = int(raw_input())         
    p = Person(age)  
    p.amIOld()
    for j in range(0, 3):
        p.yearPasses()        
    p.amIOld()
    print("")